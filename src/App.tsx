import React from 'react';
import Welcoming from "./components/welcoming";
import MainCourseOne from "./components/main-course-one";
import MainCourseTwo from "./components/main-course-two";
import SwitchCourse from "./components/switch-course";
import Cpec from "./components/cpec";
import Reviews from "./components/reviews";
import Faq from "./components/faq";
import Comate from "./components/comate";
import Footer from "./components/footer";

const App = () => (
    <>
        {/* <video autoPlay poster="./intro.gif" />

        <div className="video-block">
            <iframe
                title="intro video"
                width="853"
                height="480"
                src="https://www.youtube.com/embed/Kh1YtMER_QI?rel=0&amp;controls=0&amp;showinfo=0;autoplay=1"
                frameBorder="0"
                allowFullScreen
            ></iframe>
        </div> */}

        <Welcoming/>
        <MainCourseOne/>
        <MainCourseTwo/>
        <SwitchCourse/>
        <Cpec/>
        <Reviews/>
        <Faq/>
        <Comate/>
        <Footer/>

    </>
);


export default App;
