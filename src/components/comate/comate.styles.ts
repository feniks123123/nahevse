import styled from "styled-components";

export const ComateWrapper = styled.div`
  margin-top: 100px;
  margin-bottom: 130px;
  @media(max-width: 768px) {
    padding-left: 10px;
    margin-top: 115px;
  }
`;

export const Slide = styled.div`
  max-width: 477px;
`;

export const Img = styled.img`
  max-width: 314px;
  width: 100%;
  margin-bottom: 64px;
  @media(max-width: 768px) {
    margin-bottom: 25px;
  }
`;
