import React from 'react';
import { ComateWrapper, Slide, Img } from './comate.styles';
import {Col, Row} from "react-bootstrap";
import { Container, H2, P } from "../../stylesGlobal";
import Slider from "react-slick";
import ksyha from '../../media/img/comate/ksyha.png';
import matvei from '../../media/img/comate/matvei.png';
import leva from '../../media/img/comate/leva.png';
import ylina from '../../media/img/comate/ylina.png';

const Comate = () => {
    const settings = {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: "50px",
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    centerPadding: "15px",
                }
            }
        ]
    }
    return (
        <ComateWrapper>
            <Container>
                <Row>
                    <Col md={{ span: 9, offset: 1 }}>
                        <H2 marginBottom={56}>Со товарищи</H2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Slider {...settings}>
                            <Slide>
                                <Img src={matvei} alt=""/>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Автор, преподаватель</P>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>57ая, ИСАА МГУ, иврит, история Востока; учитель английского и зарубежной литературы.</P>
                                <P marginLeft={24} color='#41BA8A' fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Очень хотел бы отвезти ребят в
                                    прикольный музей не в Москве.</P>
                            </Slide>
                            <Slide>
                                <Img src={ylina} alt=""/>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Автор, преподаватель</P>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>
                                    Вальдорфская школа, истфак РГПУ
                                    им.&nbsp;Герцена, специалист по ВОВ. Экскурсовод, учитель истории. Преподавала в "Сириусе".
                                </P>
                                <P marginLeft={24} color='#F15B50' fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Хотела бы рассказать про каждого льва в Северной столице.</P>
                            </Slide>
                            <Slide>
                                <Img src={ksyha} alt=""/>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Преподаватель</P>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>57ая, филфак МГУ; нейролингвист, переводчик, специалист
                                    по невербальной коммуникации.</P>
                                <P marginLeft={24} color='#3E7FD6' fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Мечтает проводить международные турниры по играм, в которые
                                    играет с учениками на занятиях.</P>
                            </Slide>
                            <Slide>
                                <Img src={leva} alt=""/>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Преподаватель</P>
                                <P marginLeft={24} marginBottom={30} fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}> 57ая, истфак РАНХиГС, политфак ВШЭ. Учитель истории в московской экспериментальной школе.</P>
                                <P marginLeft={24} color='#FA99D8' fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottomMobile={20}>Жалеет, что лучшие люди уезжают
                                    из России, и надеется, что они
                                    когда-нибудь вернутся.</P>
                            </Slide>
                        </Slider>
                    </Col>
                </Row>
            </Container>
        </ComateWrapper>
    )
}

export default Comate;
