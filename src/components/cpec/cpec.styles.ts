import styled from "styled-components";

export const CpecWrapper = styled.div`
  margin-top: 170px;
  .accordion-header {
    display: flex;
    justify-content: flex-end;
    line-height: 55px;
    margin-bottom: 17px;
    > button {
      padding: 0;
      font-size: 40px;
      text-align: right;
      background: none;
      color: #353535;
      box-shadow: none;
      border: none;
      width: auto;
      &:after {
        content: none;
      }
      @media(max-width: 768px) {
        font-size: 17px;
      }
    }
    @media(max-width: 768px) {
      line-height: normal;
    }
  }
  .accordion-item:first-of-type .accordion-button {
    border: none;
  }
  .accordion-item { 
    border: none !important;
    background: none;
  }
  .accordion-body {
    padding: 0;
  }
  @media(max-width: 768px) {
    .accordion {
      margin-bottom: 87px;
    }
    margin-top: 204px;
  }
`;

export const Title = styled.h3<any>`
  font-style: normal;
  font-weight: normal;
  font-size: 90px;
  line-height: 68px;
  color: ${({ color }) => color};
  margin-bottom: 30px;
  display: flex;
  align-items: flex-end;
  @media(max-width: 1680px) {
    font-size: 52px;
    line-height: 52px;
  }
  @media(max-width: 1366px) {
    font-size: 46px;
    line-height: 46px;
  }
  @media(max-width: 1024px) {
    font-size: 42px;
    line-height: 42px;
  }
  @media(max-width: 768px) {
    font-size: 39px;
    line-height: 32px;
    margin-bottom: 12px;
  }
`;

export const ButtonHandler = styled.div<any>`
  font-size: 30px;
  line-height: 24px;
  color: ${({ color }) => color};
  cursor: pointer;
  @media(max-width: 768px) {
    font-size: 17px;
  }
`;

export const Dot = styled.div`
  margin-left: 18px;
  margin-bottom: -10px;
  @media(max-width: 768px) {
    margin-left: 15px;
    margin-bottom: -6px;
  }
`;
