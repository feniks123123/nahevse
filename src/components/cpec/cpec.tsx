import React from "react";
import { CpecWrapper, Title, ButtonHandler, Dot } from './cpec.styles';
import { Container, P } from '../../stylesGlobal';
import {Accordion, Col, Row} from "react-bootstrap";
import { handleClickOnboarding } from "../../utils";
import dot from '../../media/img/cpec/dot.svg';
import dot2 from '../../media/img/cpec/dot2.svg';
import dotMobile from '../../media/img/cpec/dot2-mob.svg';
import dot2Mobile from '../../media/img/cpec/dot-mob.svg';
import useWidthPage from "../../hook/useWidthPage";

const Cpec = () => {
    return (
        <CpecWrapper>
            <Container>
                <Row>
                    <Col lg={{ span: 5, offset: 1 }}>
                        <Title color='#F79AD6'>
                            Если<br/> сложновато
                            <Dot>
                                <img src={useWidthPage() ? dotMobile : dot} alt=""/>
                            </Dot>
                        </Title>
                        <Accordion>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>Курсы попроще +</Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16} marginBottom={30}>Если вам меньше 10 лет и разговоры
                                        о&nbsp;конституции вызывают у вас приступ чесотки, то для вас есть облегчённая версия основного курса, тоже по реги-онам, но вместо учебника — мультики.</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={28} lineHeightMobile={16} fontSizeMobile={14}>А если вы с трудом говорите или вообще почти не читаете, то вам подойдёт наш курс языковой подготовки для скорейшего набора лексики, развития речи и навыка чтения.</P>
                                    <ButtonHandler color='#F79AD6' onClick={handleClickOnboarding}>ЗАПИСАТЬСЯ ПОПРОЩЕ</ButtonHandler>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col lg={{ span: 4, offset: 1 }} >
                        <Title color='#FA9467'>
                            Если<br/> маловато
                            <Dot>
                                <img src={useWidthPage() ? dot2Mobile : dot2} alt=""/>
                            </Dot>
                        </Title>
                        <Accordion>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>Спецкурсы +</Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} marginBottom={10} lineHeightMobile={16} fontSizeMobile={14}>Правописание эпохи автокорректора</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={10} lineHeightMobile={16} fontSizeMobile={14}>Нескучные современные книги</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={10} lineHeightMobile={16} fontSizeMobile={14}>Кино/театр</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={10} lineHeightMobile={16} fontSizeMobile={14}>Разная музыка России</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={10} lineHeightMobile={16} fontSizeMobile={14}>Как менялось русское искусство</P>
                                    <P fontSize={20} lineHeight={24} marginBottom={28} lineHeightMobile={16} fontSizeMobile={14}>Языковые игры</P>
                                    <ButtonHandler color='#FA9467' onClick={handleClickOnboarding}>ЗАПИСАТЬСЯ ПОБОЛЬШЕ</ButtonHandler>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                </Row>
            </Container>
        </CpecWrapper>
    )
}

export default Cpec;
