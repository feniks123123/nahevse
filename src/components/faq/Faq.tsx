import React from 'react';
import { Accordion, Img } from './faq.styles';
import {Container, P} from "../../stylesGlobal";
import {Col, Row} from "react-bootstrap";
import logo from '../../media/img/faq/logocircle.svg';
import './faq.scss';

const Faq = () => {
    return (
        <div className="FaqWrapper">
            <div className="FaqWrapper__logo-wrapper" >
                <Img src={logo}/>
            </div>
            <Container>
                <Row>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#F79AD6'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>Сколько это стоит?</Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>
                                        Основной курс стоит $120 в месяц, а стартовый языковой  — $60 в месяц.
                                        Если через месяц регулярного посещения решите не продолжать — вернём деньги. При оплате сразу за учебный год — скидка 10%. И за каждого ребёнка, который пришел по вашей рекомендации и остался после пробного месяца, тоже скидка 10% — и вам, и ему. Ваши собственные дети тоже считаются, а несколько скидок перемножаются. <span>Открыть калькулятор</span> Если вы не можете себе позволить платить полную стоимость, но очень хотите заниматься, то по результатам интервью с ребёнком мы можем дать скидку вплоть до 100%.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#3ABE8B'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                    Что с расписанием занятий?
                                </Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>Занятия основного курса проходят 1 раз в неделю и длятся 90 минут — мы много играем, поэтому даже самые неусидчивые дети справляются. День и время согласовываются внутри групп, обычно в выходной, чтобы ребятам из обоих полушарий было удобно.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#EE5A54'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                    Есть ли у вас домашние задания?
                                </Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>Есть, но обязательная часть у них небольшая — посмотреть ~20 минут из нашего видеоучебника и пройти пару коротких тестов на понимание. А всё остальное — что-то прочитать, придумать или сделать — факультативно. Например, к каждому занятию есть музыкальный плейлист, который слушать не обязательно, но если его всё-таки послушать, то можно ответить на дополнительный вопрос на уроке и получить больше очков, чем другие ребята в группе. 🤘 А ещё мы стараемся делать домашние задания так, чтобы интересующимся родителям было тоже весело в них участвовать — в первую очередь смотреть и обсуждать учебник вместе с детьми. Мы даже приготовили для родителей более серьезные материалы, чтобы при желании вы могли дополнить наш рассказ.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#CF94F9'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                    На какой возраст рассчитаны занятия?
                                </Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>Основной курс рассчитан на детей примерно от 10 лет, но всё очень индивидуально: у нас были и ученики 8-ми лет, прекрасно справлявшиеся с материалом. Для младших детей есть облегчённая версия курса — с мультиками. Да и верхнего предела по возрасту у нас нет — наши видеоуроки с удовольствием смотрят многие взрослые, а на занятиях мы легко можем заменить часть игр на более серьёзные обсуждения.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#FA9467'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                    Занимаетесь ли вы с детьми из России?
                                </Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>Да, у нас занимаются ребята со всего света — и из России, и из всех остальных стран от Австралии до США.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                    <Col md={{ span: 9, offset: 1 }} xs={10}>
                        <Accordion color='#3E7FD6'>
                            <Accordion.Item eventKey="0">
                                <Accordion.Header>
                                    А что если мой ребёнок плохо знает русский?
                                </Accordion.Header>
                                <Accordion.Body>
                                    <P fontSize={20} lineHeight={24} fontSizeMobile={14} lineHeightMobile={16}>Даже если ребёнок с трудом разговаривает или совсем не читает, ничего страшного — просто перед основным курсом надо будет позаниматься стартовым курсом русского. Занятия в нём тоже проходят в игровой форме и нацелены на быстрый набор лексики и навыка чтения.</P>
                                </Accordion.Body>
                            </Accordion.Item>
                        </Accordion>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Faq;
