import styled, { keyframes } from "styled-components";
import { Accordion as OldAccordion } from "react-bootstrap";

export const Accordion = styled(OldAccordion)`
  .accordion-header {
    > button {
      color: ${({color}) => color};
    }
  }
`;

const logo = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`

export const Img = styled.img`
  max-width: 237px;
  width: 100%;
  height: 197px;
  animation: ${logo} 6s linear 0s normal none infinite running;
`;
