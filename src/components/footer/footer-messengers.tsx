import React from "react";
import Telegtam from './_assets/Telegtam.svg';
import WhatsApp from './_assets/WhatsApp.svg';
import FacebookMessenger from './_assets/FacebookMessenger.svg';
import Mail from './_assets/Mail.svg';
import blueRhombus from '../../media/img/footer/blueRhombus.svg';

import './footer-messengers.scss'

const list = [
    { id: 'Telegtam', src: '', icon: Telegtam },
    { id: 'WhatsApp', src: '', icon: WhatsApp },
    { id: 'FacebookMessenger', src: '', icon: FacebookMessenger },
    { id: 'Mail', src: '', icon: Mail }
]

const style = { background: `url(${blueRhombus}) no-repeat`, backgroundSize: 'cover' }

export const FooterMessengers = () => {
    return (
        <div className='FooterMessengers' >
            {list.map(({id, src, icon}) => {
                return (
                    <a className='FooterMessengers__link' key={id} href={src} style={style}>
                        <img className='FooterMessengers__img' src={icon} alt={id} />
                    </a>
                )
            })}
        </div>
    )
}

