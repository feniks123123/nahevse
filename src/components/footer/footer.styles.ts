import styled from "styled-components";
import dotline from '../../media/img/footer/dot.svg';
import dotVertical from '../../media/img/footer/dotVertical.svg';
import blueRhombus from '../../media/img/footer/blueRhombus.svg';
import zap from '../../media/img/footer/zap.svg';

export const FooterWrapper = styled.div`
  margin-bottom: 40px;
  @media(max-width: 768px) {
    padding-left: 10px;
  }
`;

export const Feedback = styled.div`
  margin-top: 67px;
  @media(max-width: 768px) {
    margin-top: 45px;
  }
`;

export const DotTop = styled.div`
  background: url(${dotline}) repeat-x;
  background-size: cover;
  height: 21px;
  flex: 1 0 auto;
  width: 25%;
  @media(max-width: 1680px) {
    width: 22%;
  }
  @media(max-width: 1024px) {
    width: 26%;
  }
  @media(max-width: 768px) {
    background-position: 10% center;
    margin-bottom: 17px;
    width: 45%;
  }
`;
export const RhombusFeedBack = styled.div`
  position: relative;
  width: 100%;
  max-width: 410px;
  min-width: 410px;
  height: 410px;
  background: url(${blueRhombus}) no-repeat;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  z-index: 1;
  margin-top: -20px;
  cursor: pointer;
  @media(max-width: 1280px) {
    margin-left: -34px;
  } 
  @media(max-width: 1024px) {
    margin-left: -24px;
  } 
  @media(max-width: 768px) {
    width: 190px;
    height: 190px;
    min-width: initial;
    min-height: initial;
    background-size: contain;
    margin-left: 0;
    margin-bottom: 120px;
  }  
`;
export const RhombusFeedBackText = styled.p`
  font-size: 40px;
  line-height: 48px;
  color: #F0E9E2;
  text-align: center;
  cursor: pointer;
  @media(max-width: 768px) {
    font-size: 17px;
  }
`;
export const DotCenter = styled.div<any>`
  background: url(${({ isMobile }) => isMobile ? dotVertical : dotline}) no-repeat;
  height: 21px;
  width: 100%;
  align-self: center;
  background-size: cover;
  margin-top: -20px;
  margin-left: -142px;
  @media(max-width: 1680px) {
    margin-left: -20px;
  }
  @media(max-width: 1366px) {
    margin-left: -14px;
  } 
  @media(max-width: 768px) {
    max-width: 100px;
    margin: 0;
    transform: rotate(90deg) translateX(-50px);
  }
`;
export const RhombusSave = styled.div`
  max-width: 410px;
  min-width: 410px;
  width: 100%;
  height: 410px;
  overflow: hidden;
  background: url(${zap}) no-repeat center;
  background-size: cover;
  margin-top: -20px;
  margin-right: 528px;
  cursor: pointer;
  @media(max-width: 1680px) {
    margin-right: 486px;
  } 
  @media(max-width: 1540px) {
    margin-right: 320px;
  } 
  @media(max-width: 1366px) {
    margin-right: 232px;
  } 
  @media(max-width: 1280px) {
    margin-right: 180px;
  }
  @media(max-width: 1024px) {
    margin-right: -20px;
  } 
  @media(max-width: 768px) {
    width: 190px;
    height: 190px;
    min-width: initial;
    margin-bottom: 17px;
  }
`;

export const DotBottom = styled.div`
  background: url(${dotline}) repeat-x;
  height: 21px;
  width: 42%;
  background-size: cover;
  margin-left: auto;
  margin-top: -22px;
  @media(max-width: 1680px) {
    width: 41%;
  }
  @media(max-width: 1540px) {
    width: 33%;
  } 
  @media(max-width: 1366px) {
    width: 32%;
  }
  @media(max-width: 1280px) {
    width: 30%;
  } 
  @media(max-width: 1024px) {
    width: 15%;
  } 
  @media(max-width: 768px) {
    width: 48%;
  } 
`;
export const WrapperMessage = styled.div`
  display: flex;
  margin-top: 69px;
  padding-bottom: 10px;
  @media(max-width: 768px) {
    margin-top: 22px;
    height: 33px;
    padding-left: 10px;
  }
`;
export const Img = styled.img`
  margin-right: 29px;
  margin-bottom: -14px;
  cursor: pointer;
  @media(max-width: 768px) {
    max-width: 33px;
    height: 33px;
    margin-right: 15px;
  }
`;

export const ColumnRomb = styled.div`
  width: 25%;
  margin-left: 10%;
  @media(max-width: 768px) {
    width: auto;
    margin-left: 0
  }
`;
