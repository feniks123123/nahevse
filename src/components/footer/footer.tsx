import React, { useState } from "react";
import { FooterWrapper, Feedback, DotTop, RhombusFeedBack, DotCenter, RhombusSave, DotBottom, RhombusFeedBackText, WrapperMessage, Img, ColumnRomb} from './footer.styles';
import {Container, P} from "../../stylesGlobal";
import {Col, Row} from "react-bootstrap";
import { handleClickOnboarding } from "../../utils";
import telega from '../../media/img/footer/telega.svg';
import fc from '../../media/img/footer/fc.svg';
import copy from '../../media/img/footer/copy.svg';
import useWidthPage from "../../hook/useWidthPage";
import { FooterMessengers } from "./footer-messengers";

import './footer.scss'

const link = 'https://nashev.se/adventure'

const Footer = () => {
    const [isVisible, setIsVisible] = useState(false)

    const handleClickCopy = () => navigator.clipboard.writeText(link)
    const handleMouseEnter = () => setIsVisible(true);
    const handleMouseLeave = () => setIsVisible(false);
    const handleClick = () => setIsVisible(prev => !prev);

    return (
        <div className='FooterWrapper overflow-hidden'>
            <Container>
                <Row>
                    <Col md={{ offset: 1 }}>
                        <P fontSize={40} lineHeight={48} marginBottom={14} fontSizeMobile={17} marginBottomMobile={0} lineHeightMobile='normal'>Покажите эту страницу ребёнку:</P>
                        <div className='d-flex align-items-center' style={{ cursor: 'pointer' }}>
                            <P fontSize={40} marginRight={16} lineHeight={48} color='#3E7FD6' fontSizeMobile={17} lineHeightMobile='normal'>{link}</P>
                            <img src={copy} alt="" onClick={handleClickCopy} />
                        </div>
                    </Col>
                </Row>
            </Container>
            {/* {!useWidthPage() && ( */}
                <Feedback>
                    <DotTop/>
                    <Container>
                        <div className='FooterWrapper__content'>
                            <ColumnRomb className=''>
                                <RhombusFeedBack onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
                                    <RhombusFeedBackText>
                                        Задать вопрос
                                    </RhombusFeedBackText>
                                    {isVisible &&
                                        <div className='FooterWrapper__messengers'>
                                            <FooterMessengers />
                                        </div>
                                    }
                                </RhombusFeedBack>
                            </ColumnRomb>
                            {/* <Col md={2} className='align-self-center' style={{ height: '21px'}}> */}
                                <DotCenter/>
                            {/* </Col>
                            <Col> */}
                                <RhombusSave onClick={handleClickOnboarding} />
                            {/* </Col> */}
                        </div>
                    </Container>
                    <DotBottom/>
                </Feedback>
            {/* )} */}
            {/* {useWidthPage() && (
                <Feedback>
                    <Row>
                        <Col xs={7}>
                            <DotTop/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ span: 7, offset: 3 }}>
                            <RhombusFeedBack onClick={handleClick}>
                                <RhombusFeedBackText>
                                    Задать вопрос
                                </RhombusFeedBackText>
                                {isVisible &&
                                    <div className='FooterWrapper__messengers'>
                                        <FooterMessengers />
                                    </div>
                                }
                            </RhombusFeedBack>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ span: 7, offset: 3 }}>
                            <DotCenter isMobile/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ span: 7, offset: 3 }}>
                            <RhombusSave/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={{ span: 7, offset: 5 }}>
                            <DotBottom/>
                        </Col>
                    </Row>
                </Feedback>
            )} */}
            <div className='FooterWrapper__mode-details'>
                <P fontSize={40} lineHeight={48} fontSizeMobile={18} color='#F15950' marginRight={25} lineHeightMobile='normal'>Здесь ещё подробнее</P>
                <Img src={telega} alt=""/>
                <Img src={fc} alt=""/>
            </div>
            <Container>
                <Row>
                    <Col md={{ span: 6, offset: 7 }} xs={12}>
                        <WrapperMessage className='align-items-center'>
                           
                        </WrapperMessage>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer;
