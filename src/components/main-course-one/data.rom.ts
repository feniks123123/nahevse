import rurik from "../../media/img/main-course-one/рюрик.png";
import vladimir from "../../media/img/main-course-one/князь_владимир.png";
import kalita from "../../media/img/main-course-one/калита.png";
import grozni from "../../media/img/main-course-one/грозный.png";
import petr1 from "../../media/img/main-course-one/петр.png";
import ekaterina from "../../media/img/main-course-one/катя.png";
import alexs1 from "../../media/img/main-course-one/саша1.png";
import alexs2 from "../../media/img/main-course-one/саша2.png";
import lenin from "../../media/img/main-course-one/ленин.png";
import stalin from "../../media/img/main-course-one/сталин.png";
import brejnev from "../../media/img/main-course-one/Брежнев.png";
import elcin from "../../media/img/main-course-one/ельцин.png";

export const romMobile = [
    '',
    {
        color: '#3E7FD6',
        img: rurik,
        name: 'Рюрик'
    },
    '',
    {
        color: '#41BA8A',
        img: vladimir,
        name: 'Владимир'
    },
    '',
    '',
    '',
    {
        color: '#CF94F9',
        img: kalita,
        name: 'Иван Калита'
    },
    '',
    '',
    {
        color: '#FA99D8',
        img: grozni,
        name: 'Иван Грозный'
    },
    '',
    '',
    {
        color: '#FA9467',
        img: petr1,
        name: 'Пётр I'
    },
    '',
    '',
    {
        color: '#41BA8A',
        img: ekaterina,
        name: 'Екатерина II'
    },
    '',
    {
        color: '#F15950',
        img: alexs1,
        name: 'Александр I'
    },
    '',
    {
        color: '#3E7FD6',
        img: alexs2,
        name: 'Александр II'
    },
    '',
    {
        color: '#F15B50',
        img: lenin,
        name: 'Ленин'
    },
    {
        color: '#FA99D8',
        img: stalin,
        name: 'Сталин'
    },
    '',
    {
        color: '#41BA8A',
        img: brejnev,
        name: 'Брежнев'
    },
    '',
    {
        color: '#FA9467',
        img: elcin,
        name: 'Ельцин'
    }
]

export const rom = [
    '',
    {
        color: '#3E7FD6',
        img: rurik,
        name: 'Рюрик'
    },
    '',
    {
        color: '#41BA8A',
        img: vladimir,
        name: 'Владимир'
    },
    '',
    '',
    '',
    '',
    '',
    {
        color: '#CF94F9',
        img: kalita,
        name: 'Иван Калита'
    },
    '',
    '',
    '',
    {
        color: '#FA99D8',
        img: grozni,
        name: 'Иван Грозный'
    },
    '',
    '',
    {
        color: '#FA9467',
        img: petr1,
        name: 'Пётр I'
    },
    '',
    '',
    {
        color: '#41BA8A',
        img: ekaterina,
        name: 'Екатерина II'
    },
    '',
    {
        color: '#F15950',
        img: alexs1,
        name: 'Александр I'
    },
    '',
    {
        color: '#3E7FD6',
        img: alexs2,
        name: 'Александр II'
    },
    '',
    {
        color: '#F15B50',
        img: lenin,
        name: 'Ленин'
    },
    {
        color: '#FA99D8',
        img: stalin,
        name: 'Сталин'
    },
    '',
    {
        color: '#41BA8A',
        img: brejnev,
        name: 'Брежнев'
    },
    '',
    {
        color: '#FA9467',
        img: elcin,
        name: 'Ельцин'
    }
]
