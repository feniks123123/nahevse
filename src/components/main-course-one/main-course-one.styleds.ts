import styled from "styled-components";

export const MainCourseWrapper = styled.div`
  margin: 0 25px 129px 40px;
  padding: 0 100px;
  > div {
    padding: 0;
  }
  @media(max-width: 768px) {
    margin: 0;
    margin-bottom: 180px;
  }
`;

export const ImgBlock = styled.div`
  display: flex;
  align-items: center;
  @media(max-width: 768px) {
    margin-top: 71px;
    margin-bottom: 60px;
  }
`;

export const SlideSectionDesktop = styled.div<any>`
  display: flex;
  width: calc(100% - 40px);
  margin: 40px auto 0;
  align-items: center;
  justify-content: center;

  @media(max-width: 1680px) {
    transform: scale(0.95)
  }

  @media(max-width: 1440px) {
    transform: scale(0.90)
  }

  @media(max-width: 768px) {
    flex-wrap: wrap;
    > div {
      margin-right: 23px;
      &:nth-child(9) {
        margin-right: 0;
        margin-bottom: 28px;
      }
      &:nth-child(10) {
        margin-right: 0;
        margin-bottom: 28px;
      }
    }
  }
`;

export const SlideSection = styled.div<any>`
  display: flex;
  width: 100%;
  margin: 40px auto 0;
  align-items: center;
  justify-content: center;

  @media(max-width: 768px) {
    flex-wrap: wrap;
    > div {
      margin-right: 23px;
      &:nth-child(9) {
        margin-right: 0;
        margin-bottom: 28px;
      }
      &:nth-child(10) {
        margin-right: 0;
        margin-bottom: 28px;
      }
    }
  }
`;

export const SlideSectionRevers = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 87%;
  > div {
    margin-right: 23px;
    &:nth-child(1) {
      margin-right: 0;
    }
    &:nth-child(9) {
      margin-bottom: 28px;
    }
    &:nth-child(10) {
      margin-bottom: 28px;
    }
  }
`;

export const RhombusWrapper = styled.div<any>`
  margin-right: 2%;
  transition: transform 0.4s ease;
  transform: scale(${({zoom}) => zoom && '2'}) translateY(${({zoom}) => zoom && '-2px'});
  @media(max-width: 768px) {
    margin-right: 0;
  }
`;

export const ColCustum = styled.div`
  width: 13px;
  height: 13px;
`;


export const ColCustumNine = styled(ColCustum)`
  width: 86%;
  text-align: right;
`;

export const WrapperText = styled.div`
  margin-right: 23%;
`;

export const Wrapper = styled.div`
  margin-bottom: 152px;
`;
