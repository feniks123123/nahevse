import React, {useEffect, useState} from 'react';
import { MainCourseWrapper, ImgBlock, SlideSection, RhombusWrapper, ColCustum, ColCustumNine, SlideSectionRevers, SlideSectionDesktop } from "./main-course-one.styleds";
import {Container, H2, P, Img} from "../../stylesGlobal";
import {Col, Row} from "react-bootstrap";
import elcin from '../../media/img/main-course-one/ельцин.png';
import RhombusSvg from "../../media/img/main-course-one/rhombus-svg";
import lastRomb from '../../media/img/main-course-one/last-romb.svg';
import useWidthPage from "../../hook/useWidthPage";
import {rom, romMobile} from "./data.rom";
import './main-course-one.scss'

type MainCurse = {
    color: string;
    img: string;
    name: string;
}

const InitMainCurse = {
    color: '#FA9467',
    img: elcin,
    name: 'Ельцин'
}

const list: any = [
    romMobile.slice(0, 10),
    [romMobile[10]],
    romMobile.slice(11, 19),
    [romMobile[19]],
    romMobile.slice(20),
]

const MainCourseOne = () => {
    const [mainCurse, setMainCurse] = useState<MainCurse>(InitMainCurse);
    const [count, setCount] = useState<number>(rom.length);

    const handlerMouseEnter = (el: any) => (event: any) => {
        const id = event.currentTarget.id;
        if(id) {
            setMainCurse(el);
            setCount(0);
        }
    }

    useEffect(() => {
        const start = setInterval(() => {
            const curse = typeof rom[count] === "object" ? rom[count] : '';
            console.log(curse);
            if(typeof curse !== "string") {
                setMainCurse(curse);
            }
            setCount(count - 1);
        }, 1500)
        return () => clearInterval(start);
    }, [count]);

    return (
        <MainCourseWrapper>
            <Container>
                <Row>
                    <Col md={{ span: 11, offset: 1 }}>
                        <H2 marginBottom={55} marginBottomMobile={53}>Основной курс</H2>
                    </Col>
                </Row>
                <Row className='align-items-center'>
                    <Col md={6}>
                        <Row className='align-items-baseline'>
                            <Col md={4} xs={3}>
                                <P fontSize={40} fontSizeMobile={16} lineHeightMobile='normal'>Часть I</P>
                            </Col>
                            <Col md={8} xs={9}>
                                <P fontSize={40} fontSizeMobile={16} lineHeightMobile={10} marginBottomMobile={5}>Исторический ликбез</P>
                                <P fontSize={20} lineHeight={24} fontSizeMobile={8} lineHeightMobile={10}>(в лицах)</P>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={{ span:5, offset: 1 }}>
                        <Row>
                            <Col md={{ span: 4, offset: 2 }} xs={{ span: 12, offset: 0 }}>
                                <ImgBlock>
                                    <Img maxWidth={177} marginRight={35} maxWidthMobile={132} marginRightMobile={29} src={mainCurse?.img} alt=""/>
                                    <P fontSize={40} fontSizeMobile={22}>{mainCurse?.name}</P>
                                </ImgBlock>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
            {!useWidthPage() && <SlideSectionDesktop>
                {rom.map((el, key) =>
                    <RhombusWrapper key={key}
                                    zoom={typeof el !== "string" && el.name === mainCurse?.name}
                                    id={typeof el !== "string" ? el.name : el}
                                    onMouseEnter={handlerMouseEnter(el)}>
                        <RhombusSvg color={typeof el !== "string" ? el.color : el}/>
                    </RhombusWrapper>
                )}
                <Img maxWidth={128} src={lastRomb} alt=""/>
            </SlideSectionDesktop> }
            {useWidthPage() && (
                <Container>
                    <Row>
                        <Col>
                            {list.map((rombsli: Array<any>) => (
                                <div className="MainCourseOne__rombs-list">
                                    {rombsli.map((el, key) => {
                                        return (
                                            <ColCustum key={key}>
                                                <RhombusWrapper zoom={typeof el !== "string" && el.name === mainCurse?.name}
                                                                id={typeof el !== "string" ? el.name : el}
                                                                onMouseEnter={handlerMouseEnter(el)}>
                                                    <RhombusSvg color={typeof el !== "string" ? el.color : el} size={13}/>
                                                </RhombusWrapper>
                                            </ColCustum>
                                        )
                                    })}
                                </div>
                            ))}
                        </Col>
                    </Row>
                </Container>
            )}
        </MainCourseWrapper>
    )
}

export default MainCourseOne;
