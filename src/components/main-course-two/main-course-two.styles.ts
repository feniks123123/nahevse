import styled from "styled-components";
import zap from '../../media/img/footer/zap.svg';

export const MainCourseTwoWrapper = styled.div`
  padding: 100px 0 500px;
  position: relative;

  > div {
    padding: 0;
  }
  @media(max-width: 768px) {
    margin-bottom: 111px;
    padding-left: 10px;
  }
`;

export const TextWrapper = styled.div`
  height: 107px;
  margin-bottom: 50px;
  display: flex;
  @media(max-width: 768px) {
    margin-top: 57px;
    max-width: 290px;
  }
`;

export const WrapperText = styled.div`
  margin-right: 10%;
`;


export const MainWrapper = styled.div`
  overflow: hidden;
`;

export const Map = styled.div`
  height: 100%;
  max-height: 738px;
  width: 100%;
  position: relative;
  @media(max-width: 768px) {
    scroll-behavior: smooth;
    overflow: scroll hidden;
    -webkit-overflow-scrolling: touch;
  }
`;

export const MapContainer = styled.div`
  margin-top: 50px;
`;


export const RombScroll = styled.div<any>`
  width: 150px;
  height: 150px;
  background: url(${zap}) no-repeat center;
  background-size: cover;
  transform: ${({ transform }) => transform};
  left: ${({ left }) => left || '-100px'};
  position: absolute;
  bottom: 200px;
  overflow: hidden;
`;
