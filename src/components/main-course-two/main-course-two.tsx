import React, {useEffect, useRef, useState} from 'react';
import { TextWrapper, MainWrapper, Map, RombScroll } from './main-course-two.styles';
import {Col, Row} from "react-bootstrap";
import {Container, P} from "../../stylesGlobal";
import { handleClickOnboarding } from "../../utils";
import Main from '../../media/img/main-course-two/map';
import useWidthPage from "../../hook/useWidthPage";
import ReactDOM from 'react-dom';
import './main-course-two.scss'

const map = [
    {
        id: 'severZapadRussia',
        title: `<span>Северо-Запад</span> — западный форпост`,
        color: '#F79364',
    },
    {
        id: 'moskva',
        title: `<span>Москва</span> — почему&nbsp;первопрестольная и почему нерезиновая`,
        color: '#EE5A54',
    },
    {
        id: 'centerRussia',
        title: `<span>Центральная Россия</span> — золотое замкадье России`,
        color: '#3E7FD6',
    },
    {
        id: 'sibir',
        title: `<span>Сибирь </span> — недра, кедры и мороз`,
        color: '#3E7FD6',
    },
    {
        id: 'severRussia',
        title: `<span>Русский Север</span> — деревянные церкви и бараки`,
        color: '#41BA8A',
    },
    {
        id: 'ugRussia',
        title: `<span>Юг России</span> — казаки и субтропики`,
        color: '#41BA8A',
    },
    {
        id: 'piter',
        title: `<span>Санкт-Петербург</span> — величие Империи в провинции`,
        color: '#F15B50',
    },
    {
        id: 'severZapadRussia',
        title: `<span>Северо-Запад</span> — западный форпост`,
        color: '#F79364',
    },
    {
        id: 'povolojie',
        title: `<span>Поволжье</span> — мусульмане и торговля`,
        color: '#F79364',
    },
    {
        id: 'ural',
        title: `<span>Урал</span> — что производят русские заводы`,
        color: '#FA99D8',
    },
    {
        id: 'crim',
        title: `<span>Крым</span> — все ответы на вопросы “чей?”`,
        color: '#FA99D8',
    },
    {
        id: 'kavkaz',
        title: `<span>Кавказ</span> — (не)покоренные горцы`,
        color: '#CF94F9',
    },
    {
        id: 'dalniVostok',
        title: `<span>Дальний Восток</span> — Европа далеко, а Азия близко`,
        color: '#F15B50',
    },
]

const calculateScale = ({ scrollPercent }: any) => {
    const middle = (scrollPercent / 50 )

    return scrollPercent <= 50 ? (middle * 2) : (1 + (2 - middle))
}

// const calculateTranslateY = ({ scrollPercent, y }: any) => {
//     const firstHalf = 650 +(150 * (scrollPercent / 100));
//     const secondHalf = 650 + ((150 * ((50 - (scrollPercent - 50)) / 100)));

//     return scrollPercent <= 50 ? firstHalf : -(y) ;
// }

const MainCourseTwo = () => {
    const containerRef = useRef<any>()
    const rombRef = useRef<any>()

    const [rombStyles, setStyles] = useState<any>({transform: `scale(0) translate(0px, 0px)`});
    const [currentActive, setCurrentActive] = useState<string>('');
    const getMap = map.find((el) => el.id === currentActive);

    const handlerScroll = () => {
        setTimeout(() => {
            const element: any = ReactDOM.findDOMNode(containerRef.current);

            if(element) {
                const { y, width, } = element.getBoundingClientRect();

                if(y <= 0 && (-(y)) < element.offsetHeight ) {
                    const scrollPercent = Math.round((100 * (-(y)) / (element.offsetHeight - 110)));
    
                    const scale = calculateScale({ scrollPercent });
                    const left = Math.round(((width * (scrollPercent / 100)) ) - 100) + 30;
    
                    // const bottom = calculateTranslateY({ scrollPercent, y });
                    setStyles({
                        left: `${left}px`,
                        // bottom: `${bottom}px`,
                        // transform: `scale(${2.0}) translateY(${translateY}px)`
                        transform: `scale(${scale})`
                    })
                } else {
                    setStyles({
                        transform: `scale(0)`
                    })
                }
            }
        }, 0)
    }

    useEffect(() => {
        document.addEventListener('scroll', handlerScroll)
    }, []);

    return (
        <div className="MainCourseTwoWrapper" ref={containerRef}>
            <Container>
                <Row>
                    <Col md={2} xs={3}>
                        <P fontSize={40} fontSizeMobile={16} lineHeightMobile='normal'>Часть II</P>
                    </Col>
                    <Col md={4} xs={9}>
                        <P fontSize={40} fontSizeMobile={16} lineHeightMobile={10} marginBottomMobile={5}>Вся Россия</P>
                        <P fontSize={20} lineHeight={24} fontSizeMobile={8} lineHeightMobile={10}>(история/география/культура)</P>
                    </Col>
                    {!useWidthPage() &&
                        <Col md={6}>
                            <TextWrapper>
                                <P fontSize={40} fontSizeMobile={16} lineHeightMobile={20} spanColor={getMap?.color} dangerouslySetInnerHTML={{
                                    // @ts-ignore
                                    __html: getMap?.title
                                }}/>
                            </TextWrapper>
                        </Col>
                     }
                </Row>
                <Row className='justify-content-center'>
                    {!useWidthPage() && <Main setCurrentActive={setCurrentActive}/>}
                </Row>
            </Container>
            {useWidthPage() && (
                <MainWrapper>
                    <Map>
                        <Main setCurrentActive={setCurrentActive}/>
                    </Map>
                </MainWrapper>
            )}
            <RombScroll ref={rombRef} {...rombStyles} onClick={handleClickOnboarding} />
        </div>
    )
}

export default MainCourseTwo;
