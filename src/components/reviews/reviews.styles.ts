import styled from "styled-components";
import Slider from "react-slick";
import dotline from '../../media/img/reviews/dotline.svg';

export const ReviewsWrapper = styled.div`
  margin-top: 210px;
  margin-bottom: 175px;
  @media(max-width: 768px) {
    margin-top: 113px;
  }
`;

export const Title = styled.h2`
  font-size: 60px;
  line-height: 74px;
  color: #353535;
  margin-bottom: 25px;
  @media(max-width: 768px) {
    font-size: 20px;
    line-height: 26px;
    margin-bottom: 58px;
    padding-left: 10px;
  }
`;

export const NewSlider = styled(Slider)``;

export const RhombusSlide = styled.div`
  background: url(${dotline}) repeat-x;
  width: 100%;
  height: 20px;
  margin-bottom: 22px;
  margin-top: 22px;
`;

export const Slide = styled.div<any>`
  background: ${({ bg }) => `url(${bg}) no-repeat;`};
  background-size: cover;
  border-radius: 157px;
  height: 462px;
  margin: 0 16px;
  position: relative;
  width: 524px !important;

  @media(max-width: 768px) {
    max-width: 222px;
    width: 222px;
    height: 196px;
    border-radius: 60px;
  }
`;

export const Review = styled.div`
  font-size: 20px;
  line-height: 24px;
  color: #EEE8E0;
  max-width: 221px;
  position: absolute;
  right: 2%;
  top: 45%;

  @media(max-width: 768px) {
    font-size: 10px;
    line-height: 14px;
    width: 40%;
    top: initial;
    top: 45%;
  }
`;

export const Name = styled.div`
  margin-bottom: 38px;
  font-size: 60px;
  line-height: 74px;
  color: #EEE8E0;
  position: absolute;
  right: 12%;
  top: 5%;
  > p {
    color: #EEE8E0;
    margin-left: 20px;
    font-size: 40px;
  }

  @media(max-width: 768px) {
    font-size: 32px;
    line-height: 36px;
    margin-bottom: 18px;
    > p {
      font-size: 20px;
    }
  }
`;
