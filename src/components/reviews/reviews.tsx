import React from "react";
import { ReviewsWrapper, Title, NewSlider, RhombusSlide, Slide, Review, Name } from './reviews.styles';
import {Container, P} from "../../stylesGlobal";
import {Col, Row} from "react-bootstrap";
import aron from '../../media/img/reviews/aron.png'
import dasha from '../../media/img/reviews/dasha.png'
import glasha from '../../media/img/reviews/glasha.png'
import katya from '../../media/img/reviews/kata.png'
import misha from '../../media/img/reviews/misha.png'
import olya from '../../media/img/reviews/olya.png'
import ruslan from '../../media/img/reviews/ruslan.png'
import tim from '../../media/img/reviews/tim.png'
// import { ReactComponent as PlayButton } from "../../media/img/switch-course/playbutton.svg";
import { PlayButton } from "../switch-course/switch-course.styles";

const Reviews = () => {
    const settings = {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        centerMode: true,
        slidesToShow: 4,
        rows: 1,
        variableWidth: true,
        slidesToScroll: 4,
        centerPadding: "120px",
        responsive: [
            {
                breakpoint: 1921,
                settings: {
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    centerPadding: "70px",
                }
            }
        ]
    };

    const slides: Array<any> = [
        { name: 'Даша,', color: '#3A7DDA', country: 'США', bg: dasha, youtube: 'https://youtu.be/bCiqGjF9yic', review: '“Со всеми друзьями говорим по-русски”',  age: '13 лет' },
        { name: 'Арон,', color: '#FBBFC0', country: 'Англия', bg: aron, youtube: 'https://youtu.be/wR2NRHwLqRk', review: '“У меня есть две страсти — теннис и Россия”',  age: '9 лет' },
        { name: 'Тим,', color: '#F15B50', country: 'США', bg: tim, youtube: 'https://youtu.be/Pzuh3ujpMUI', review: '“Вместе со мной училось много других очень прикольных детей”',  age: '11 лет' },
        { name: 'Руслан,', color: '#3A7DDA', country: 'Испания', bg: ruslan, youtube: 'https://youtu.be/RpXgQO-AJnE', review: '“Во время игр ты вроде бы и отдыхаешь, но продолжаешь учиться”',  age: '15 лет' },
        { name: 'Катюша,', color: '#3A7DDA', country: 'США', bg: katya, youtube: 'https://youtu.be/p21Tj1GiErQ', review: '“Видео открывали двери для разговоров про историю моей семьи”',  age: '15 лет' },
        { name: 'Глаша,', color: '#3A7DDA', country: 'Канада', bg: glasha, youtube: 'https://youtu.be/humxx478Ut8', review: '“Я нарисовала короткий мультик про Рязань”',  age: '12 лет' },
        { name: 'Оля,', color: '#F15B50', country: 'США', bg: olya, youtube: 'https://youtu.be/_v96JxuZXE4', review: '“Мне нравится, что мы узнаём много про географию России”',  age: '10 лет' },
        { name: 'Миша,', color: '#3ABE8B', country: 'США', bg: misha, youtube: 'https://youtu.be/7EpERhoywiw', review: '“Преподаватели делают уроки очень интересными”',  age: '12 лет' },
    ]
    return (
        <ReviewsWrapper>
            <Container>
                <Row>
                    <Col md={{ offset: 1 }}>
                        <Title>
                            Мы запустились год назад.<br/>
                            Вот что говорят те, кто уже с нами:
                        </Title>
                    </Col>
                </Row>
            </Container>
            <RhombusSlide/>
                <NewSlider {...settings}>
                    {slides.map(({ bg, youtube, age, name, review, color }) => {
                        return (
                            <Slide bg={bg} onClick={() => (window.location.href = youtube)}>
                                <PlayButton color={color} />
                                <Name>
                                    {name}<br/>
                                    <p>{age}</p>
                                </Name>
                                <Review>
                                    {review}
                                </Review>
                            </Slide>
                        )
                    })}
                </NewSlider>
            <RhombusSlide/>
        </ReviewsWrapper>
    )
}

export default Reviews;
