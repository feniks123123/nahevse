import styled from "styled-components";
import rhombus from './../../media/img/switch-course/rhombus.svg';
import greenRhombus from './../../media/img/switch-course/greenRhombus.svg';
import purpleRhombus from './../../media/img/switch-course/purpleRhombus.svg';
import cerkov from './../../media/img/switch-course/церковь.png';
import line from './../../media/img/switch-course/косая_линейка.png';
import { ReactComponent as PlayButtonSVG } from "../../media/img/switch-course/playbutton2.svg";

export const SwitchCourseWrapper = styled.div`
  position: relative;
  background-size: contain;
  height: 100%;
  max-height: 818px;
  background: url(${rhombus}) center repeat-x;
  @media(max-width: 768px) {
    background: none;
  }
`;

export const ContainerSwitchCourse = styled.div`
  height: 920px;
`;

export const SwitchRhombus = styled.div<any>`
  width: 100%;
  max-width: 818px;
  max-height: 818px;
  height: 100%;
  background: url(${({activeCourse}) => activeCourse ? purpleRhombus : greenRhombus}) center no-repeat;
  background-size: contain;
  position: relative;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;
  @media(max-width: 768px) {
    width: 323px;
    height: 323px;
  }
`;

export const VideoBlock = styled.div<any>`
  width: 100%;
  max-width: 524px;
  max-height: 295px;
  height: 100%;
  border-radius: 85px;
  margin: 0 auto;
  background: url(${({ bg }) => bg ? cerkov : line}) #F0E9E2 no-repeat center;
  background-size: cover;
  display: flex;
  flex-direction: column;
  position: relative;
  > img {
    &:nth-child(2) {
      margin-left: 53px;
      margin-top: 44px;
      margin-bottom: 52px;
    }
    }
    &:nth-child(3) {
      text-align: center;
    }
  @media(max-width: 768px) {
    max-width: 212px;
    height: 119px;
    border-radius: 40px;
    > img {
      &:nth-child(2) {
        margin-bottom: 21px;
        margin-top: 17px;
        margin-left: -85px;
      }
      &:nth-child(3) {
        text-align: center;
      }
  }
`;

export const PlayButton = styled(PlayButtonSVG)<any>   `
  cursor: pointer;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 58px;
  z-index: 3;
  transform: translate(-50%, -50%);
  -webkit-filter: drop-shadow( 12px 4px 16px rgba(0,0,0,.7));
  filter: drop-shadow( 12px 4px 16px rgba(0,0,0,.7));
  fill: ${({ color }) => color};

  @media(max-width: 768px) {
    width: 23px;
  }
`;

const Button = styled.div<any>`
  position: absolute;
  font-size: calc(29px + (90 - 29) * ( (100vw - 375px) / (1920 - 375) ));
  line-height: 90px;
  color: ${({ active }) => !active ? 'rgba(53, 53, 53, 0.5)' : '#353535'};
  cursor: pointer;
`;

export const ButtonRhombusLeft = styled(Button)`
  transform: rotate(-45deg) translateY(-50%);
  top: 17%;
  left: 1%;
  @media(max-width: 768px) {
    top: 40px;
    left: 10px;
    line-height: normal;
  }
`;

export const ButtonRhombusRight = styled(Button)`
  transform: rotate(45deg) translateY(-50%);
  top: 17%;
  right: 1%;
  @media(max-width: 768px) {
    line-height: normal;
  }
`;

