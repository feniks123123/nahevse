import React, {useCallback, useState} from "react";
import {
    SwitchCourseWrapper,
    SwitchRhombus,
    VideoBlock,
    PlayButton,
    ButtonRhombusLeft,
    ButtonRhombusRight,
    ContainerSwitchCourse
} from './switch-course.styles';
import logo from './../../media/img/switch-course/logo.svg';
import sever from './../../media/img/switch-course/course.svg'
import arhan from './../../media/img/switch-course/arhan.svg'
import overiewCourse from './../../media/img/switch-course/overiew-course.svg'
import demo from './../../media/img/switch-course/demo.svg'

import play from './../../media/img/switch-course/playbutton.svg';
import { Img, Container, P } from '../../stylesGlobal';
import {Col, Row} from "react-bootstrap";
import useWidthPage from "../../hook/useWidthPage";

const SwitchCourse = () => {
    const [buttonActive, setButtonActive] = useState('left');

    const handlerButton = useCallback((event) => {
      const name = event.currentTarget.id;
        setButtonActive(name)
    }, [])

    const hansleClickVideo = () => {
        window.location.href =  buttonActive === 'left' ? 'https://youtu.be/RNpPRnbqwFU' : 'https://youtu.be/szbKvwRWTAc';
    }

    const isButton = buttonActive === 'left';
    
    return (
        <ContainerSwitchCourse>
            <SwitchCourseWrapper>
                <SwitchRhombus activeCourse={!isButton}>
                    <ButtonRhombusLeft active={isButton} onClick={handlerButton} id='left'>Учебник</ButtonRhombusLeft>
                    <ButtonRhombusRight active={!isButton} onClick={handlerButton} id='right'>Занятия</ButtonRhombusRight>
                    <VideoBlock bg={isButton} onClick={hansleClickVideo} >
                        <PlayButton src={play}  />
                        <Img src={logo} maxWidth={177} maxWidthMobile={72} alt=""/>
                        <Img src={isButton ? sever : demo} maxWidth={isButton ? 201 : 64} maxWidthMobile={82} marginBottom={6.27} margin='0 auto' alt=''/>
                        <Img src={isButton ? arhan : overiewCourse} maxWidth={isButton ? 388 : 450} maxWidthMobile={157} margin='0 auto' alt=''/>
                    </VideoBlock>
                </SwitchRhombus>
            </SwitchCourseWrapper>
            {!useWidthPage() && <Container style={{ marginTop: '-80px' }}>
                <Row>
                    <Col xs={{ span: 3, offset: 2 }}>
                        <P fontSize={27} marginTopMobile={39} lineHeightMobile={20} visibility={!isButton}>Это глава из нашего видеоучебника про&nbsp;Россию. Всего их около ста (пока).<br/>
                            Со временем мы расскажем ещё и
                            про все бывшие республики СССР.</P>
                    </Col>
                    <Col xs={{ span: 3, offset: 2 }}>
                        <P fontSize={27} lineHeightMobile={20} visibility={isButton}>Занятия проходят на интерактивной доске Miro и бывают очень разными. У нас есть
                            и задания в игровой форме, и игры с эле-
                            ментами заданий, и даже просто игры.</P>
                    </Col>
                </Row>
            </Container>}
            {useWidthPage() && <Container>
                <Row>
                    <Col xs={12}>
                        {isButton ?
                            <P fontSizeMobile={14} marginTopMobile={44} lineHeightMobile={20} paddingMobile='0 0 0 10px'>
                                Это глава из нашего видеоучебника про Россию. Всего их около ста (пока).<br/>
                                Со временем мы расскажем ещё и про все бывшие республики СССР.</P>
                            :
                            <P fontSizeMobile={14} marginTopMobile={44} lineHeightMobile={20} paddingMobile='0 0 0 10px'>
                                Занятия проходят на интерактивной доске Miro и бывают очень разными. У нас есть
                                и задания в игровой форме, и игры с элементами заданий, и даже просто игры.
                            </P>
                        }
                    </Col>
                </Row>
            </Container>}
            
        </ContainerSwitchCourse>
    )
}

export default SwitchCourse;
