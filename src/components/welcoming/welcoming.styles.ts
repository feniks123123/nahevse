import styled from "styled-components/";
import rhombusBg from './../../media/img/welcoming/rhombus.png';
import romb from './../../media/img/welcoming/romb.svg';
import rhombushorizontal from './../../media/img/welcoming/rhombushorizontal.svg';
import vector from './../../media/img/welcoming/vector.svg';
import { P } from '../../stylesGlobal';

export const VideoWrapper = styled.div`
  width: 100%;
  height: 100%;
  max-height: 592px;
  border-radius: 130px;
  position: relative;
  @media(max-width: 768px) {
    height: 264px;
    max-width: 100%;
    border-radius: 60px;
  }
`;

export const IframeWrapper = styled.div`
  overflow: hidden;
  height: 100%;
`;

export const RhombusVideo = styled.div`
  background: url(${rhombusBg}) no-repeat;
  transform: translateX(-50%);
  background-size: contain;
  width: 100%;
  max-width: 300px;
  height: 300px;
  position: absolute;
  bottom: -147px;
  left: 50%;
  z-index: 1;
  > p {
    line-height: 36px;
    text-align: center;
    left: 50%;
    transform: translate(-50%, -50%);
    position: absolute;
    top: 50%;
  }
  @media(max-width: 768px) {
    width: 125px;
    height: 125px;
    bottom: -62px;
    > p {
      font-size: 12px;
      line-height: 16px;
    }
  }
`;

export const Logo = styled.div`
  margin-bottom: 36px;
  margin-top: 70px;
  @media(max-width: 768px) {
    margin-bottom: 38px;
  }
  @media(max-width: 375px) {
    width: 336px;
    margin: 70px 0 38px 0;
  }
`;

export const Description = styled(P)`
  margin-bottom: 42px;
  line-height: 36px;  
  max-width: 713px;
  @media(max-width: 768px) {
    margin-bottom: 25px;
    line-height: 20px;
    letter-spacing: 0.04em;
  }
`;

export const RhombusColumn = styled.div<any>`
  background: url(${document.getElementsByTagName('body')[0].clientWidth < 767 ? rhombushorizontal : romb}) no-repeat center;
  height: 100%;
  width: 25px;
  margin-left: auto;
  @media(max-width: 768px) {
    width: 100%;
    height: 14px;
    background-size: cover;
    margin: ${({ margin }) => margin ? margin : '85px 0 0 0'};
  }
`;

export const WelcomingWrapper = styled.div`
  margin-bottom: 184px;
  height: 100%;
  padding: 0 100px;
  @media(max-width: 768px) {
    margin-bottom: 108px;
  }
  @media(max-width: 375px) {
    width: 336px;
    margin: 0 auto 184px;
    padding: 0;
  }
`;

export const CursiveText = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: flex-end;
  text-align: right;
  margin-left: auto;
  padding-right: 10px;
  font-family: 'Kobzar', serif;
  font-size: calc(32px + (61 - 32) * ( (100vw - 375px) / (1920 - 375) ));
  line-height: 64px;
  text-transform: lowercase;
  color: #EE5A54;
  margin-bottom: 65px;
  @media(max-width: 1680px) {
    margin-bottom: 32px;
  }
  @media(max-width: 1440px) {
    margin-bottom: 64px;
  }
  @media(max-width: 768px) {
    background: url(${vector}) no-repeat;
    line-height: 34px;
    max-width: 300px;
    padding-top: 10px;
    margin-bottom: 29px;
    margin-right: 13px;
  }
`;

export const ContentLeft = styled.div`
    height: 100%;
`;
