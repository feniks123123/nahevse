import React, { useState } from "react";
import {Row} from "react-bootstrap";
import { VideoWrapper, RhombusVideo, Logo, Description, RhombusColumn, WelcomingWrapper, IframeWrapper, CursiveText, ContentLeft} from "./welcoming.styles";
import logo from './../../media/img/welcoming/logo.png';
import index from '../../media/img/welcoming/индекс.png';
import fullIndex from '../../media/img/welcoming/full-index.png';
import { Container, H3, P, Img, Col } from '../../stylesGlobal';
import { openPopupWidget } from "react-calendly";
import './welcoming.scss'
import useWidthPage from "../../hook/useWidthPage";

const Welcoming = () => {
    const [indexSrc, setIndexSrc] = useState(index);

    const handleMouseEnter = () => {
        setIndexSrc(fullIndex)
    }

    const handleMouseLeave = () => {
        setIndexSrc(index)
    }

    const handleClickOnboarding = () => {
        openPopupWidget({ url: 'https://calendly.com/nashevse/intro?background_color=e5e5e5&text_color=353535&primary_color=f15950' })
    }

    return (
        // <WelcomingWrapper>
        //     <Container className='h-100'>
        //         <Row className='h-100' xs={12}>
        //             <Col md={6} xs={12}>
        //                 <Row className='h-100'>
        //                     <Col md={11}>
        //                         <ContentLeft>
        //                             <Logo>
        //                                 <Img maxWidth={758} src={logo} alt=""/>
        //                             </Logo>
        //                             <Description fontSize={30} fontSizeMobile={16} lineHeight={40}>Школа русского языка и культуры для детей
        //                                 и&nbsp;подростков со всего мира</Description>
        //                             <VideoWrapper>
        //                                 <IframeWrapper>
        //                                     <iframe src="https://www.videoask.com/f75t08d8k"
        //                                             allow="camera *; microphone *; autoplay *; encrypted-media *; fullscreen *; display-capture *;"
        //                                             width="100%"
        //                                             height="100%" style={{ border:'none', borderRadius: '80px'}}/>
        //                                 </IframeWrapper>
        //                                 <RhombusVideo>
        //                                     <P fontSize={30} fontSizeMobile={12} lineHeightMobile={16} lineHeight={36}>Это<br/>
        //                                         интерактивное<br/>
        //                                         видео!</P>
        //                                 </RhombusVideo>
        //                             </VideoWrapper>
        //                         </ContentLeft>
        //                     </Col>
        //                     <Col md={1}>
        //                         <RhombusColumn/>
        //                     </Col>
        //                 </Row>
        //             </Col>
        //             <Col md={6} xs={12}>
        //                 <H3 marginBottom={200} marginLeft={50} paddingTop={175} marginTopMobile={45} marginBottomMobile={45} paddingMobile='0 0 0 10px'>
        //                     Поделитесь с детьми русской культурой.
        //                     Она классная!
        //                 </H3>
        //                 <P marginBottom={55} marginLeft={45} fontSizeMobile={16} fontSize={30} lineHeightMobile={18} paddingMobile='0 0 0 10px'>Рассказываем о России так,<br/>
        //                     что хочется</P>
        //                 <CursiveText>
        //                     от  Волги  до Енисея ногами  считать километры.
        //                 </CursiveText>
        //                 <div className='text-end' onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} onClick={handleClickOnboarding}>
        //                     <Img src={indexSrc} maxWidthMobile={316} maxWidth={628} alt=""/>
        //                 </div>
        //             </Col>
        //         </Row>
        //     </Container>
        // </WelcomingWrapper>
        <div className="Welcoming__wrapper">
            <div className="Welcoming__left-side">
                <Row className='h-100' xl={12}>
                    <Col md={11} >
                        <ContentLeft>
                            <Logo>
                                <Img maxWidth={758} src={logo} alt=""/>
                            </Logo>
                            <Description fontSize={30} fontSizeMobile={16} lineHeight={40}>Школа русского языка и культуры для детей
                                и&nbsp;подростков со всего мира</Description>
                            <VideoWrapper>
                                <IframeWrapper>
                                    <iframe src="https://www.videoask.com/f75t08d8k"
                                            allow="camera *; microphone *; autoplay *; encrypted-media *; fullscreen *; display-capture *;"
                                            width="100%"
                                            height="100%" style={{ border:'none', borderRadius: '80px'}}/>
                                </IframeWrapper>
                                <RhombusVideo>
                                    <P fontSize={30} fontSizeMobile={12} lineHeightMobile={16} lineHeight={36}>Это<br/>
                                        интерактивное<br/>
                                        видео!</P>
                                </RhombusVideo>
                            </VideoWrapper>
                        </ContentLeft>
                    </Col>
                    <Col md={1}>
                        <RhombusColumn/>
                    </Col>
                </Row>
            </div>
            <div className="Welcoming__right-side">
                <H3 
                    marginBottom={200}
                    marginBottomMobile={45}
                    marginLeft={50}
                    marginLeftMobile={0}
                    marginTopMobile={45}
                    paddingMobile='0 0 0 10px'
                    paddingTop={175}
                >
                    Поделитесь с детьми русской культурой.
                    Она классная!
                </H3>
                <P
                    marginBottom={55}
                    marginLeft={45}
                    fontSizeMobile={16}
                    marginLeftMobile={0}
                    marginBottomMobile={8}
                    fontSize={30}
                    lineHeightMobile={18}
                    paddingMobile='0 0 0 10px'
                >
                    Рассказываем о России так,<br/>
                    что хочется</P>
                <CursiveText>
                    от  Волги  до Енисея ногами  считать километры.
                </CursiveText>
                {useWidthPage() ? (
                    <div className='text-end' onClick={handleClickOnboarding}>
                        <Img src={fullIndex} maxWidthMobile={316} maxWidth={628} alt=""/>
                    </div>
                ): (
                    <div className='text-end' onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} onClick={handleClickOnboarding}>
                        <Img src={indexSrc} maxWidthMobile={316} maxWidth={628} alt=""/>
                    </div>
                )}
                
            </div>
        </div>
    )
}

export default Welcoming;
