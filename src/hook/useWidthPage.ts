const useWidthPage = (width?: string | number) => document.getElementsByTagName('body')[0].clientWidth < (!!width ? width : 767);

export default useWidthPage;
