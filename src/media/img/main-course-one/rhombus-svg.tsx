import React from 'react';

const RhombusSvg = ({ color, size }: any) => (
    <svg width={size ? size : '21'} height={size ? size : '21'} viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M10.6927 0.570068L0.342773 10.7401L10.6927 21.0001L20.8628 10.7401L10.6927 0.570068Z" fill={color ? color : "#353535"}/>
    </svg>
)

export default RhombusSvg;
