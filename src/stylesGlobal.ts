import styled, { createGlobalStyle } from "styled-components";
import { Container as OldContainer, Col as OldCol } from "react-bootstrap";

export const GlobalStyle = createGlobalStyle`
  body {
    background: #F0E9E2;
    font-family: 'Aeroport', sans-serif;  
    overflow: auto;
  }
  p {
    color: #353535;
    margin-bottom: 0;
  }

  .slick-track {
    display: flex;
    flex-wrap: nowrap;
  }
`
export const Container = styled(OldContainer)`
  max-width: 1712px;
  position: relative;
  @media(max-width: 375px) {
    width: 336px;
    max-width: 336px;
  }
`;

type H3Type = {
    marginBottom?: number | string;
    marginTop?: number | string;
    marginTopMobile?: number | string;
    marginBottomMobile?: number | string;
    marginLeftMobile?: number | string;
    marginLeft?: number | string;
    paddingMobile?: string;
    paddingTop?: string | number;
}

export const H3 = styled.h3<H3Type>`
  margin-bottom: ${({ marginBottom }) => marginBottom}px;
  margin-top: ${({ marginTop }) => marginTop}px;
  margin-left: ${({ marginLeft }) => marginLeft}px;
  padding-top: ${({ paddingTop }) => paddingTop}px;
  font-size: calc(28px + (60 - 28) * ( (100vw - 375px) / (1920 - 375) ));
  line-height: 74px;
  color: #353535;
  @media(max-width: 1280px) {
    padding-top: 160px;
    margin-bottom: 160px;
  }
  @media(max-width: 768px) {
    line-height: 42px;
    margin-bottom: ${({ marginBottomMobile }) => marginBottomMobile}px;
    margin-left: ${({ marginLeftMobile }) => marginLeftMobile}px;
    margin-top: ${({ marginTopMobile }) => marginTopMobile}px;
    padding: ${({paddingMobile}) => paddingMobile};
  }
`;

type Type = {
    marginBottom?: string | number;
    marginTop?: string | number;
    marginRight?: string | number;
    marginLeft?: string | number;
    maxWidth?: string | number;
    lineHeight?: number | string;
    fontSize?: number | string;
    visibility?: boolean;
    color?: string;
    spanColor?: string;
    fontSizeMobile?: number | string;
    lineHeightMobile?: number | string;
    paddingMobile?: string;
    maxWidthMobile?: string | number;
    marginBottomMobile?: string | number;
    marginRightMobile?: number | string;
    marginTopMobile?: number | string;
    marginLeftMobile?: number | string;
    margin?: string;
}
export const H2 = styled.h2<Type>`
  margin-bottom: ${({ marginBottom }) => marginBottom}px;
  margin-top: ${({ marginTop }) => marginTop}px;
  margin-left: ${({ marginLeft }) => marginLeft}px;
  font-size: calc(37px + (90 - 37) * ((100vw - 375px) / (1920 - 375)));
  line-height: 90px;
  color: #353535;
  @media(max-width: 768px) {
    line-height: normal;
    margin-bottom: ${({ marginBottomMobile }) => marginBottomMobile}px;
    margin-left: ${({ marginLeftMobile }) => marginLeftMobile}px;
  }
`;

export const P = styled.p<Type>`
  font-size: ${({ fontSize, fontSizeMobile }) => `calc(${fontSizeMobile}px + (${fontSize} - ${fontSizeMobile}) * ( (100vw - 375px) / (1920 - 375) ))`};
  margin-top: ${({ marginTop }) => marginTop}px;
  margin-right: ${({ marginRight }) => marginRight}px;
  margin-left: ${({ marginLeft }) => marginLeft}px;
  margin-bottom: ${({ marginBottom }) => marginBottom}px;
  line-height: ${({ lineHeight }) => typeof lineHeight === 'string' ? lineHeight : `${lineHeight}px`};
  max-width: ${({ maxWidth }) => maxWidth}px;
  color: ${({ color }) => color};
  ${({visibility}) => visibility ? 'visibility: hidden' : 'visibility: visible'};
  > span {
    color: ${({ spanColor }) => spanColor};
  }
  @media(max-width: 768px) {
    font-size: ${({ fontSizeMobile }) => fontSizeMobile}px;
    line-height: ${({ lineHeightMobile }) => typeof lineHeightMobile === 'string' ? lineHeightMobile : `${lineHeightMobile}px`};
    padding: ${({ paddingMobile }) => paddingMobile};
    margin-top: ${({ marginTopMobile }) => marginTopMobile}px;
    margin-bottom: ${({ marginBottomMobile }) => marginBottomMobile}px;
    margin-left: ${({ marginLeftMobile }) => marginLeftMobile}px;
  }
  @media(max-width: 375px) {
    width: 336px;
  }
`;

export const Img = styled.img<Type>`
  width: 100%;
  max-width: ${({ maxWidth }) => maxWidth}px;
  margin-bottom: ${({ marginBottom }) => marginBottom}px;
  margin-top: ${({ marginTop }) => marginTop}px;
  margin-left: ${({ marginLeft }) => marginLeft}px;
  margin-right: ${({ marginRight }) => marginRight}px;
  margin: ${({ margin }) => margin};
  @media(max-width: 768px) {
    max-width: ${({ maxWidthMobile }) => maxWidthMobile}px;
    margin-right: ${({ marginRightMobile }) => marginRightMobile}px;
  }
  @media(max-width: 375px) {
    width: 336px;
  }
`;

export const Col = styled(OldCol)`
  padding: 0;
`;
