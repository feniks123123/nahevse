import { openPopupWidget } from "react-calendly";

export const handleClickOnboarding = () => {
    openPopupWidget({ url: 'https://calendly.com/nashevse/intro?background_color=e5e5e5&text_color=353535&primary_color=f15950' })
}